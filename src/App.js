import React from 'react';
import './App.css';

import Form1 from './Form1'
import Form2 from './Form2'


function App() {
  return (
    <div className="App">
      <h3>Test page</h3>
                <p>Потрібно навчитися перед сабмітом збирати дані із різних компонентів,
                     перевіряти і у разі успішності відправляти на сервер</p>
                <p>Для цього, я створив дві не залежні форми і один загальний сабміт</p>
                <p>Потрібна функція, яка буде викликати алерт із даними з двох форм</p>
                <p>Також треба врахувати, що в реальності десь може бути затримка у виконанні</p>
                <div style={{display:'flex', flexDirection:'row', justifyContent:'center'}}>
                    <div style={{border:"1px solid black", 
                         margin:'10px 10px 10px 10px',
                         padding:'10px 10px 10px 10px'}}>
                        <Form1 />
                    </div>
                    <div style={{border:"1px solid black", 
                         margin:'10px 10px 10px 10px',
                         padding:'10px 10px 10px 10px'}}>
                        <Form2 />
                    </div>
                </div>
                <div style={{display:'flex', flexDirection:'row', justifyContent:'center'}}>
                   <button>General Submit</button>
                </div>
    </div>
  );
}

export default App;
