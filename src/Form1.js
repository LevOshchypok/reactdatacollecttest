import React, {Component} from 'react'


class Form1 extends Component{
    constructor(props){
        super(props)

        this.state = {
            name1:'',
            age1:''
        }
    }

    handleSubmit = (e) => {
        alert(JSON.stringify(this.state))
    }

    handleChange = (e) => {
        this.setState({[e.target.id]: e.target.value})
    }

    render(){
        return (
            <React.Fragment>
                <b> Form1 </b><br/>
                <form style={{paddingLeft:'10px'}}>
                    <label htmlFor="name1">Name1</label><br/>
                    <input id="name1" 
                           type="text" 
                           onChange={this.handleChange}/><br/>
                    <label htmlFor="age1">Age1</label><br/>
                    <input id="age1" 
                           type="text"
                           onChange={this.handleChange}/><br/>
                    <button onClick={this.handleSubmit}>submit</button>
                </form>
            </React.Fragment>
        )
    }
}


export default Form1