import React, {Component} from 'react'


class Form2 extends Component{
    constructor(props){
        super(props)

        this.state = {
            name2:'',
            age2:''
        }
    }

    handleSubmit = (e) => {
        alert(JSON.stringify(this.state))
    }

    handleChange = (e) => {
        this.setState({[e.target.id]: e.target.value})
    }

    render(){
        return (
            <React.Fragment>
                <b> Form2 </b><br/>
                <form style={{paddingLeft:'10px'}}>
                    <label htmlFor="name2">Name2</label><br/>
                    <input id="name2" 
                           type="text" 
                           onChange={this.handleChange}/><br/>
                    <label htmlFor="age2">Age2</label><br/>
                    <input id="age2" 
                           type="text"
                           onChange={this.handleChange}/><br/>
                    <button onClick={this.handleSubmit}>submit</button>
                </form>
            </React.Fragment>
        )
    }
}


export default Form2